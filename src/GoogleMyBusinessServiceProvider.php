<?php

namespace Quantox\GoogleMyBusiness;

use Google_Client;
use Google_Service_MyBusiness as MyBusiness;
use Illuminate\Support\ServiceProvider;

class GoogleMyBusinessServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

    }

    /**
     * Register the application services.
     *
     * @param \Google_Client $client
     * @return void
     */
    public function register(Google_Client $client)
    {
        $this->app->singleton(MyBusiness::class, function ($client) {
            return new MyBusiness($client);
        });

        $this->app->alias(MyBusiness::class, 'GoogleMyBusiness');
    }
}