# Google My Business API v4 PHP Client library

PHP library for Google My Business API v4.

## Requirements

Google API PHP Client, version 1.1.1  
URL: https://github.com/google/google-api-php-client/releases

Illuminate/support  
URL: https://github.com/illuminate/support

## Install

In PIM's composer.json file add the following properties

``` bash
"repositories": [
    {
      "type": "package",
      "package": {
        "name": "nikola.zivkovic/gmb",
        "version": "0.0.1",
        "type": "package",
        "source": {
          "url": "https://gitlab.com/nikola.zivkovic/gmb.git",
          "type": "git",
          "reference": "master"
        }
      }
    }
  ],
  "require": {
        "nikola.zivkovic/gmb": "*"
  },
  "autoload": 
  {
      "psr-4": {
        "Quantox\\GoogleMyBusiness\\": "vendor/nikola.zivkovic/gmb/src"
      }
  },
```

In PIM's config/app.php providers array, add the following line:

```
Quantox\GoogleMyBusiness\GoogleMyBusinessServiceProvider::class,
```

